import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.RaspiPin;


public class PruebaVisual extends JFrame implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2914832131649153561L;
	private JPanel jcontentPane;
	private JToggleButton jtb00;
	private JToggleButton jtb01;
	private JToggleButton jtb02;
	private JToggleButton jtb03;
	private JToggleButton jtb04;	
	private GpioPinDigitalOutput pin00;
	private GpioPinDigitalOutput pin01;
	private GpioPinDigitalOutput pin02;
	private GpioPinDigitalOutput pin03;
	private GpioPinDigitalOutput pin04;
	
	public PruebaVisual(){
		iniciarGPIO();
		initialize();
	}
	
	private void initialize(){
		this.setContentPane(getJContentPane());
		this.setSize(600, 80);
	}
	
	private JPanel getJContentPane(){
		if(jcontentPane == null){
			jcontentPane = new JPanel();
			jcontentPane.setLayout(new GridLayout(1,5));
			jcontentPane.add(getJTB00());
			jcontentPane.add(getJTB01());
			jcontentPane.add(getJTB02());
			jcontentPane.add(getJTB03());
			jcontentPane.add(getJTB04());
		}
		return jcontentPane;
	}
	
	private JToggleButton getJTB00(){
		if(jtb00 == null){
			jtb00 = new JToggleButton();
			jtb00.setText("Pin 00");
			jtb00.addActionListener(this);
		}
		return jtb00;
	}
	
	private JToggleButton getJTB01(){
		if(jtb01 == null){
			jtb01 = new JToggleButton();
			jtb01.setText("Pin 01");
			jtb01.addActionListener(this);
		}
		return jtb01;
	}
	
	private JToggleButton getJTB02(){
		if(jtb02 == null){
			jtb02 = new JToggleButton();
			jtb02.setText("Pin 02");
			jtb02.addActionListener(this);
		}
		return jtb02;
	}
	
	private JToggleButton getJTB03(){
		if(jtb03 == null){
			jtb03 = new JToggleButton();
			jtb03.setText("Pin 03");
			jtb03.addActionListener(this);
		}
		return jtb03;
	}
	
	private JToggleButton getJTB04(){
		if(jtb04 == null){
			jtb04 = new JToggleButton();
			jtb04.setText("Pin 04");
			jtb04.addActionListener(this);
		}
		return jtb04;
	}
	
	private void iniciarGPIO(){
		GpioController gpio = GpioFactory.getInstance();
		pin00 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_00);
		pin01 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01);
		pin02 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_02);
		pin03 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_03);
		pin04 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(getJTB00())){
			if(getJTB00().isSelected()) pin00.high();
			else pin00.low();
		}
		if(e.getSource().equals(getJTB01())){
			if(getJTB01().isSelected()) pin01.high();
			else pin01.low();
		}
		if(e.getSource().equals(getJTB02())){
			if(getJTB02().isSelected()) pin02.high();
			else pin02.low();
		}
		if(e.getSource().equals(getJTB03())){
			if(getJTB03().isSelected()) pin03.high();
			else pin03.low();
		}
		if(e.getSource().equals(getJTB04())){
			if(getJTB04().isSelected()) pin04.high();
			else pin04.low();
		}
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				PruebaVisual thisClass = new PruebaVisual();
				thisClass.setLocationRelativeTo(null);
				thisClass.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				thisClass.setVisible(true);
				thisClass.setTitle("Prueba de GPIO Visual");
			}
		});
	}
}
