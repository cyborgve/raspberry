import java.awt.EventQueue;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.RaspiPin;


public class Semaforo {
	
	private final GpioController gpio = GpioFactory.getInstance();
	
	private final GpioPinDigitalOutput pin00 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_00);
	private final GpioPinDigitalOutput pin01 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01);
	private final GpioPinDigitalOutput pin02 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_02);
	private final GpioPinDigitalOutput pin03 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_03);
	private final GpioPinDigitalOutput pin04 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04);
	
	private void encenderLuz(GpioPinDigitalOutput pin){
		pin.high();
	}
	
	private void apagarluz(GpioPinDigitalOutput pin){
		pin.low();
	}
	
	private void intermitente(GpioPinDigitalOutput pin, int segs){
		float time = 0;
		pin.low();
		while(time < segs){
			pin.pulse(50,true);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}time += 0.100;
		}
	}
	
	public Semaforo(){
		try {
			intermitente(pin00, 1);
			Thread.sleep(1000);
			intermitente(pin01, 1);
			Thread.sleep(1000);
			intermitente(pin02, 1);
			Thread.sleep(1000);
			intermitente(pin03, 1);
			Thread.sleep(1000);
			intermitente(pin04, 1);
			Thread.sleep(1000);
						
			encenderLuz(pin00);
			
			iniciar();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private void iniciar(){
		for(int i = 0; i < 3; i++){
			try {
				apagarluz(pin01);
				encenderLuz(pin03);
				encenderLuz(pin04);
				Thread.sleep(10000);
				apagarluz(pin04);
				Thread.sleep(5000);
				apagarluz(pin03);
				encenderLuz(pin02);
				Thread.sleep(5000);
				apagarluz(pin02);
				encenderLuz(pin01);
				Thread.sleep(20000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		pin00.low();
		pin01.low();
		pin02.low();
		pin03.low();
		pin04.low();
		intermitente(pin00, 1);
		intermitente(pin01, 1);
		intermitente(pin02, 1);
		intermitente(pin03, 1);
		intermitente(pin04, 1);
		gpio.shutdown();
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				new Semaforo();				
			}
		});
	}
}
